# Class: nrgzfs::private::packages
# ===============================
#
# Private class to handle prerequisite package installation for nrgzfs module.
#
# Parameters
# ----------
#
# Parameters for this module.
#
# NONE
#
# Variables
# ----------
#
# Variables used by this module.
#
# * `nrgzfs::opencsw_pkg_provider`
#  The package provider to use when installing OpenCSW packages.  Default: pkgutil.
#
# Examples
# --------
#
# @example
#    class { '::nrgzfs::private::packages': }
#
# Authors
# -------
#
# Chip Schweiss <chip.schweiss@wustl.edu>
#
# Copyright
# ---------
#
# Copyright 2016 Chip Schweiss, unless otherwise noted.
#

class nrgzfs::private::packages (
  Boolean $use_kmod = true,  
){
  
  # Install prereq packages, based on OS
  case $::osfamily {
    'Solaris' : {
      case $::operatingsystem {
        'Solaris',
        'OmniOS' : {

          # OmniOS packages
          $omnios_pkg_list = [ 'mpathadm',
                               'pci.ids',
                               'pciutils' ]
          
          $pgksrc_pkg_list = [ 'mosh',
                               'bash-completion' ];
          
          package {
            $omnios_pkg_list:
              provider => 'nrgillumos_pkg',
              ensure   => present;
            $pgksrc_pkg_list:
              ensure =>  latest,
              #install_options => ['-y'],
              provider        => 'pkgin';
          }
        }
        
                
        default : {
          fail("Unsupported os: ${::operatingsystem}")
        }
      } 
      # case $::operatingsystem
    }
    
    'RedHat' : {

        if $use_kmod {
          $kmod = '1'
          $dkms = '0'
        } else {
          $kmod = '0'
          $dkms = '1'
        }

        exec { 
          'zfs-kmod':
            command => "sed -i -e \"/\[zfs-kmod\]/,/^\[/s/enabled=0/enabled=${kmod}/\" /etc/yum.repos.d/zfs.repo",
            refreshonly => true,
            require => Package["zfs-release"];
          'zfs-dkms':
            command => "sed -i -e \"/\[zfs\]/,/^\[/s/enabled=1/enabled=${dkms}/\" /etc/yum.repos.d/zfs.repo",
            refreshonly => true,
            require => Package["zfs-release"];
          'modprobe-zfs':
            command     => '/sbin/modprobe zfs',
            refreshonly => true;
        }

        file {
          '/etc/modules-load.d/zfs.conf':
            content => "zfs\n";
        }
                
        package {
          'zfs-release':
            provider => rpm,
            ensure => installed,
            notify => Exec[ "zfs-kmod", "zfs-dkms" ],
            source => "http://download.zfsonlinux.org/epel/zfs-release.el7_${facts['os']['release']['minor']}.noarch.rpm";
          'zfs':
            require => Exec[ "zfs-kmod", "zfs-dkms" ],
            notify  => Exec[ "modprobe-zfs" ],
            ensure  => present;
        }
    }

    default: {
      fail("Unsupported osfamily: ${::osfamily}")
    }
  } 
  # case $::osfamily
}
