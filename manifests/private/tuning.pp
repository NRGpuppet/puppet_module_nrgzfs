# Class: nrgzfs::private::tuning
# ===============================
#
# Private class to handle tuning some system OS parameters.
# 
# For OmniOS, this includes writing template content to /etc/system, adding a note about 
# required reboot to the MOTD, and modifying values in /kernel/drv/scsi_vhci.conf and
# /kernel/drv/sd.conf.
#
# Parameters
# ----------
#
# NONE
#
# Variables
# ----------
#
# Variables used by this class.
#
# * `nrgzfs::tuning_exec_data`
#  A hash to pass as $data to create_resources('exec', $data, $defaults), for OS tweaks
#  and tunings.  Default undef.
#
# * `nrgzfs::tuning_exec_defaults`
#  A hash to pass as $defaults to create_resources('exec', $data, $defaults), for OS tweaks
#  and tunings.  Default undef.
#
# Examples
# --------
#
# @example
#    class { '::nrgzfs::private::tuning': }
#
# Authors
# -------
#
# Chip Schweiss <chip.schweiss@wustl.edu>
#
# Copyright
# ---------
#
# Copyright 2016 Chip Schweiss, unless otherwise noted.
#

class nrgzfs::private::tuning {

  # Create any exec resources passed as params to nrgzfs
  if ($::nrgzfs::tuning_exec_data and $::nrgzfs::tuning_exec_defaults) {
    create_resources('exec', $::nrgzfs::tuning_exec_data, $::nrgzfs::tuning_exec_defaults)
  }
  elsif ($::nrgzfs::tuning_exec_data) {
    create_resources('exec', $::nrgzfs::tuning_exec_data)
  }
  
  # Do further OS-specific tweaks,
  case $::osfamily {
    'Solaris' : {
      case $::operatingsystem {
        'Solaris',
        'OmniOS' : {

          # Under OmniOS, write template to /etc/system
          file { '/etc/system':
            content => template('nrgzfs/omnios_etc_system.erb'),
            notify => Exec['etc_system_reboot_notice'],
          }
          # TODO: Module to better handle motd manipulation like this
          # Notify of need to reboot, if puppetmotd directory exists.
          # Files created in /tmp will get wiped on reboot
          $puppetmotd_path = "${::settings::confdir}/puppetmotd"
          exec { 
            'puppetmotd_mkdir':
              command => "mkdir -p ${puppetmotd_path}",
              creates => $puppetmotd_path,
              notify  =>  Exec[ 'etc_system_reboot_notice' ];
              
            'etc_system_reboot_notice':
              command => "echo 'Reboot needed due to changes to /etc/system' > /tmp/update_etc_system_motd.txt ; \
              ln -s /tmp/update_etc_system_motd.txt ${puppetmotd_path}/update_etc_system",
              user => 'root',
              creates  => '/tmp/etc_system_pending',
              refreshonly => true,
          }

          file_line {
            'remove_dma_uid':
              path              => '/etc/passwd',
              match             => '^dma:x:26*',
              match_for_absence => true,
              ensure            => absent,
              notify            => Exec[ 'etc_passwd_notice' ];
          }

          exec {
            'etc_passwd_notice':
              command     => "echo 'Reboot needed due to changes to /etc/passwd' > /tmp/update_etc_passwd_motd.txt ; \
              ln -s /tmp/update_etc_passwd_motd.txt ${puppetmotd_path}/update_etc_passwd",
              user        => 'root',
              creates     => '/tmp/etc_passwd_pending',
              refreshonly =>  true;
          }


          # Update /kernel/drv/scsi_vhci.conf
          file { '/kernel/drv/scsi_vhci.conf':
            content => template('nrgzfs/omnios_kernel_drv_scsi_vhci.conf.erb')
          }

          # Update /kernel/drv/sd.conf
          file { '/kernel/drv/sd.conf':
            content => template('nrgzfs/omnios_kernel_drv_sd.conf.erb')
          }

        }
        default : {
            fail("Unsupported os: ${::operatingsystem}")
        }
      } # case $::operatingsystem
    }
    'RedHat' : {
      # Any RedHat/Centos tuning
    }
    default: {
      fail("Unsupported osfamily: ${::osfamily}")
    }
  } # case $::osfamily
  
}
