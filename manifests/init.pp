# Class: nrgzfs
# ===========================
#
# Full description of class nrgzfs here.  This class depends on the ozmt/ozmt module.
# For OmniOS, it presently assumes OpenCSW tool pkgutil is already installed.
#
# Parameters
# ----------
#
# Parameters for this module.
#
# * `samba_from_source`
#  Whether CIFS will be served by Samba built from source instead of the OS supplied
#  version.  Default: false.
#
# * `install_santools`
#  Whether to include the nrgzfs::santools class and install Santools.  Default: false.
#
# * `install_rsf_1`
#  Whether to include the nrgzfs::santools class and install RSF-1.  Default: false.
#
# * `opencsw_pkg_provider`
#  The 'provider' attribute to use for installing OpenCSW packages.  Only relevant under
#  OmniOS.  Default 'pkgutil'
#
# * `tuning_exec_data`
#  Optional, a hash to pass as $data to create_resources('exec', $data, $defaults), for OS tweaks
#  and tunings.  Default undef.
#
# * `tuning_exec_defaults`
#  Optional, a hash to pass as $defaults to create_resources('exec', $data, $defaults), for OS 
#  tweaks and tunings.  Default undef.
#
# * `class_dependencies`
#  Array of class names, so this class can declare itself dependent on those classes.
#  E.g. [ 'Postfix' ] for [ Class['Postfix'] ].  Default undef.
#
# Variables
# ----------
#
# NONE
#
# Examples
# --------
#
# @example
#    class { '::nrgzfs':
#      install_santools => true,
#      tuning_exec_data => {
#        'set_nfs_servers' => {
#          'command' => 'sharectl set -p servers=512 nfs',
#          'onlyif' => "test `sharectl get -p servers nfs | cut -d '=' -f 2` -ne 512",
#        },
#      },
#      tuning_exec_defaults => {
#        'user' => 'root',
#      }
#      class_dependencies => [ 'Postfix' ],
#    }
#
# Authors
# -------
#
# Chip Schweiss <chip.schweiss@wustl.edu>
#
# Copyright
# ---------
#
# Copyright 2016 Chip Schweiss, unless otherwise noted.
#
class nrgzfs (
  Boolean $samba_from_source                  = false,
  Boolean $install_santools                   = false,
  Boolean $install_rsf_1                      = false,
  String $opencsw_pkg_provider                = 'pkgutil',
  Optional[Hash] $tuning_exec_data            = undef,
  Optional[Hash] $tuning_exec_defaults        = undef,
  Optional[Array] $class_dependencies         = undef,
  ) {

  # Declare dependencies, if needed
  if $class_dependencies {
    $class_dependencies.each |String $the_class| {
      Class[$the_class] -> Class['Nrgzfs']
    }
  }

  # Install prereq packages (including mercurial)
  include '::nrgzfs::private::packages'
  Class['Nrgzfs::Private::Packages'] -> Class['Ozmt']

  # Do OS tuning
  include '::nrgzfs::private::tuning'

  # Populate /etc/mercurial files
  file {
    '/etc/mercurial':
      mode   => '755',
      ensure => directory;
    '/etc/mercurial/hgrc':
      mode   => '644',
      source => 'puppet:///modules/nrgzfs/hgrc';
  }
  File['/etc/mercurial/hgrc'] -> Class['Ozmt']

  # Include OZMT, needed for RSF-1
  include '::ozmt'
  
  if $install_santools {
    # Assumed nrgzfs::santools params retrieved from hiera
    include '::nrgzfs::santools'
  }
    
  if $install_rsf_1 {
    # Assumed nrgzfs::rsf_1 params retrieved from hiera
    include '::nrgzfs::rsf_1'

    # Copy some files over from ozmt
    $ozmt_email_to = $::ozmt::email_to
    exec {
      'rsf_1_copy_S98cache':
        command => "cp ${::ozmt::ozmt_install_dir}/rsf-1/S98cache /opt/HAC/RSF-1/etc/rc.appliance.c/",
        creates => '/opt/HAC/RSF-1/etc/rc.appliance.c/S98cache',
        require => [ Class['Ozmt'], Class['Nrgzfs::Rsf_1'] ];
    }
    ->
    file{ '/opt/HAC/RSF-1/etc/rc.appliance.c/K03cache':
      target => '/opt/HAC/RSF-1/etc/rc.appliance.c/S98cache',
      ensure => link,
    }
    ->
    file { '/opt/HAC/RSF-1/etc/rc.appliance.c/S98notify_email':
      ensure => present,
      content => template('nrgzfs/S98notify_email');
    }
    ->
    file { '/opt/HAC/RSF-1/etc/rc.appliance.c/K02notify_email':
      ensure => link,
      target => '/opt/HAC/RSF-1/etc/rc.appliance.c/S98notify_email';
    }  
  }

  if $samba_from_source {
    include '::nrgzfs::samba_from_source'
  } 

}
